﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollower : MonoBehaviour {

	public float turnSpeed = 0.4f;
	public Transform PlayerTransform;
	Camera mycam;

	private Vector3 cameraOffset;
	private GameManager myGameManager;

	public float SmoothFactor = 0.5f;
	// Use this for initialization
	void Start () {
		cameraOffset = transform.position - PlayerTransform.position;
	}
	
	// Update is called once per frame
	void Update () {
		Vector3 newPos = PlayerTransform.position + cameraOffset;

		transform.position = Vector3.Lerp (transform.position, newPos, SmoothFactor);
	}
}
