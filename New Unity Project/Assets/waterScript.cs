﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class waterScript : MonoBehaviour {
	float particalSystemTimer = 3f;

	public GameObject particleSystem;
	public GameObject TreeObject;
	public Color balloonColor;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	private void Update(){
		particalSystemTimer -= Time.deltaTime;
		print (particalSystemTimer);
		if (particalSystemTimer < 0) {
			disablePartical ();
			spawnTree ();
		}
	}


	//disable the partical effect 
	public void disablePartical(){
		particleSystem.SetActive (false);
	}

	//spawn the tree
	public void spawnTree(){
		Vector3 newPosition = new Vector3 (transform.position.x, 0f, transform.position.z);
		GameObject tree = Instantiate (TreeObject, newPosition, TreeObject.transform.rotation) as GameObject;
		tree.GetComponent < firTreeScript > ().changeColor (balloonColor);
	}

}
