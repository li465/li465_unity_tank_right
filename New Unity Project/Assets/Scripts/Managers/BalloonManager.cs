﻿using System;
using UnityEngine;
using System.Collections;


[Serializable]
public class BalloonManager {
	public Color m_BalloonColor;            
	public Transform m_BalloonSpawnPoint;  
	[HideInInspector] public GameObject m_BalloonInstance;   
	[HideInInspector] public Renderer rend;


	// Use this for initialization

	private Balloon m_Balloon;


	public void Setup()
	{
		m_Balloon = m_BalloonInstance.GetComponent<Balloon>();


	}


	void Start () {
		
	}

	// Update is called once per frame
	void Update () {
		
	}

	public void Reset()
	{
		m_BalloonInstance.transform.position = m_BalloonSpawnPoint.position;
		m_BalloonInstance.transform.rotation = m_BalloonSpawnPoint.rotation;

	}




}
