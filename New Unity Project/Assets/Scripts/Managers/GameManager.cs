﻿using UnityEngine;
using System.Collections;
//using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public int m_NumRoundsToWin = 3;    
	public float m_randomX;
	public float m_randomZ;
    public float m_StartDelay = 3f;         
    public float m_EndDelay = 3f;           
    public CameraControl m_CameraControl;   
    public Text m_MessageText;              
    public GameObject m_TankPrefab;         
    public TankManager[] m_Tanks; 

	  
	public BalloonManager[] m_Balloon;
	public GameObject m_BalloonPrefab; 


    private int m_RoundNumber;              
    private WaitForSeconds m_StartWait;     
    private WaitForSeconds m_EndWait;       
    private TankManager m_RoundWinner;
	private TankManager m_ScoreWinner;
    private TankManager m_GameWinner;

	public GameObject[] tanks;

	float timeSpawnBalloon = 10f;


    private void Start()
    {
        m_StartWait = new WaitForSeconds(m_StartDelay);
        m_EndWait = new WaitForSeconds(m_EndDelay);

        SpawnAllTanks();
        //SetCameraTargets();
		SpawnAllBalloon ();

        StartCoroutine(GameLoop());
    }
		

	void Update(){
		timeSpawnBalloon -= Time.deltaTime;
		if (timeSpawnBalloon < 0) {
			SpawnAllBalloon ();
		}

	}

    private IEnumerator GameLoop()
    {
        yield return StartCoroutine(RoundStarting());
        yield return StartCoroutine(RoundPlaying());
        yield return StartCoroutine(RoundEnding());

       if (m_GameWinner != null)
        {
			Application.LoadLevel (Application.loadedLevel);
        }
        else
        {
            StartCoroutine(GameLoop());
    	}
	}


    private IEnumerator RoundStarting()
    {
		ResetAllTanks ();
		DisableTankControl ();

		//m_CameraControl.SetStartPositionAndSize ();

		m_RoundNumber++;
		m_MessageText.text = "ROUND " + m_RoundNumber;
        yield return m_StartWait;
    }


    private IEnumerator RoundPlaying()
    {
		EnableTankControl ();

		m_MessageText.text = "";

		while (!OneTankLeft ()) {
			
        	yield return null;
		}
    }


    private IEnumerator RoundEnding()
    {
		DisableTankControl ();

		m_RoundWinner = null;

		m_RoundWinner = GetRoundWinner (); //this function checks the round result, winner or draw.

		if (m_RoundWinner != null) {
			m_RoundWinner.m_Wins++;
		}

		m_ScoreWinner = null;

		m_ScoreWinner = GetScoreWinner();

		if (m_ScoreWinner != null) {
			m_ScoreWinner.m_Wins++;
		}

		m_GameWinner = GetGameWinner (); //this function checks whether there is a game winner already

		string message = EndMessage ();
		m_MessageText.text = message;

        yield return m_EndWait;
    }



    private bool OneTankLeft()
    {
        int numTanksLeft = 0;

        for (int i = 0; i < m_Tanks.Length; i++)
        {
            if (m_Tanks[i].m_Instance.activeSelf)
                numTanksLeft++;
        }

        return numTanksLeft <= 1;
    }

	//m_RoundWinner.m_scores = scoreScript.redScore;


	//this function checks the round result, winner or draw.
    private TankManager GetRoundWinner()
    {
        for (int i = 0; i < m_Tanks.Length; i++)
        {
            if (m_Tanks[i].m_Instance.activeSelf)
                return m_Tanks[i];
        }

        return null;
    }


	//this function checks whether there is a score winner already
    private TankManager GetGameWinner()
    {
        for (int i = 0; i < m_Tanks.Length; i++)
        {
            if (m_Tanks[i].m_Wins == m_NumRoundsToWin)
                return m_Tanks[i];
        }

        return null;
    }

	private TankManager GetScoreWinner(){
		if (scoreScript.redScore >= 5) {
			m_ScoreWinner = m_Tanks [1];
			m_ScoreWinner.m_Wins++;

			RoundEnding ();

		}

		if (scoreScript.blueScore >= 5) {
			m_ScoreWinner = m_Tanks [0];
			m_ScoreWinner.m_Wins++;

			RoundEnding ();

		}

		return m_ScoreWinner;
	
	
	}



    private string EndMessage()
    {
        string message = "DRAW!";

        if (m_RoundWinner != null)
            message = m_RoundWinner.m_ColoredPlayerText + " WINS THE ROUND!";

        message += "\n\n\n\n";

        for (int i = 0; i < m_Tanks.Length; i++)
        {
            message += m_Tanks[i].m_ColoredPlayerText + ": " + m_Tanks[i].m_Wins + " WINS\n";
        }

        if (m_GameWinner != null)
            message = m_GameWinner.m_ColoredPlayerText + " WINS THE GAME!";

        return message;
    }


    private void ResetAllTanks()
    {
		
        for (int i = 0; i < m_Tanks.Length; i++)
        {
            m_Tanks[i].Reset();
        }
    }


    private void EnableTankControl()
    {
        for (int i = 0; i < m_Tanks.Length; i++)
        {
            m_Tanks[i].EnableControl();
        }
    }


    private void DisableTankControl()
    {
        for (int i = 0; i < m_Tanks.Length; i++)
        {
            m_Tanks[i].DisableControl();
        }
    }

	//instantiate all of the balloons
	private void SpawnAllBalloon (){
		for (int i = 0; i < m_Balloon.Length; i++)
		{
			m_randomX = Random.Range (-30.0f, 30.0f);
			m_randomZ = Random.Range (-30.0f, 30.0f);

			Vector3 RandomPosition = new Vector3 (m_randomX, 10f, m_randomZ);
			m_Balloon[i].m_BalloonInstance =
				Instantiate(m_BalloonPrefab, RandomPosition, m_Balloon[i].m_BalloonSpawnPoint.rotation) as GameObject;
			m_Balloon [i].Setup ();
		}
		timeSpawnBalloon = 10f;
	}

	//instantiate all of the tanks
	private void SpawnAllTanks()
	{
		for (int i = 0; i < m_Tanks.Length; i++)
		{
			m_Tanks [i].m_Instance =
				tanks [i];
			m_Tanks[i].m_PlayerNumber = i + 1;
			m_Tanks[i].Setup();
		}
	}


}