﻿using UnityEngine;
using UnityEngine.UI;

public class TankShooting : MonoBehaviour
{
    public int m_PlayerNumber = 1; //seperate player 1, player2, fire1, fire2     
    public Rigidbody m_Shell; //reference to the shell object for instantiate           
    public Transform m_FireTransform; //The position will fire the shell  
    public Slider m_AimSlider;           
    public AudioSource m_ShootingAudio;  
    public AudioClip m_ChargingClip;     
    public AudioClip m_FireClip;         
    public float m_MinLaunchForce = 15f; 
    public float m_MaxLaunchForce = 30f; 
    public float m_MaxChargeTime = 0.75f; //how long from the minimum charge to the maximum charge
	public Transform turret;

   
    private string m_FireButton;  //set up the input to trigger fire       
    private float m_CurrentLaunchForce; 
    private float m_ChargeSpeed;         
    private bool m_Fired;                


    private void OnEnable()
    {
        m_CurrentLaunchForce = m_MinLaunchForce;
        m_AimSlider.value = m_MinLaunchForce;
    }


    private void Start()
    {
		//transform.position = new Vector3(Random.Range(-5f, 5f), Random.Range(6f, 8f), Random.Range(-5f, 5f));

        m_FireButton = "Fire" + m_PlayerNumber; // this will make computer know player1 for fire1, and it's relative fire trigger input

        m_ChargeSpeed = (m_MaxLaunchForce - m_MinLaunchForce) / m_MaxChargeTime;
    }
    

    private void Update()
    {
        // Track the current state of the fire button and make decisions based on the current launch force.
		m_AimSlider.value = m_MinLaunchForce;//set up the default value for the slider

		if (m_CurrentLaunchForce >= m_MaxLaunchForce && !m_Fired) {
			//at max charge, not yet fired
			m_CurrentLaunchForce = m_MaxLaunchForce;
			Fire ();

		} else if (Input.GetButtonDown (m_FireButton)) {
			//have we pressed fire for the first time?
			m_Fired = false;
			m_CurrentLaunchForce = m_MinLaunchForce;
			m_ShootingAudio.clip = m_ChargingClip; //audio play when start charging.

		} else if (Input.GetButton (m_FireButton) && !m_Fired) {
			//Holding the fire button, not yet fired

			turret.localRotation = Quaternion.Euler (new Vector3 (Mathf.MoveTowards (turret.localRotation.eulerAngles.x, -90f, Time.deltaTime * 45f), 0f, 0f));


			m_CurrentLaunchForce +=m_ChargeSpeed * Time.deltaTime;
			m_AimSlider.value = m_CurrentLaunchForce;
		} else if (Input.GetButtonUp (m_FireButton) && !m_Fired) {
			//we relaesed the button, having not fired yet
			Fire();
		}
    }


    private void Fire()
    {
        // Instantiate and launch the shell.
		m_Fired = true;

		Rigidbody shellInstance = Instantiate (m_Shell, m_FireTransform.position, m_FireTransform.rotation) as Rigidbody;
		////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
		shellInstance.velocity = m_CurrentLaunchForce * m_FireTransform.forward; //this controls the shoooting direction and distance

		shellInstance.GetComponent<ShellExplosion> ().owner = m_PlayerNumber;

		m_ShootingAudio.clip = m_FireClip;
		m_ShootingAudio.Play ();

		m_CurrentLaunchForce = m_MinLaunchForce; //reset the power back to the minimum 

		turret.localRotation = Quaternion.Euler (0f, 0f, 0f);

    }
}