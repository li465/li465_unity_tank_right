﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Balloon : MonoBehaviour {

	public float downSpeed = 0.1f;

	private Rigidbody rb;

	public Renderer rend;
	public GameObject particleSystem;
	public GameObject TreeObject;
	public GameObject TankObject;

	//get a reference of the firTreeScript
	private firTreeScript myScript;
	private TankHealth healthScript;


	int RandomNumber;


	float particalSystemTimer = 3f;

	private Color[] BalloonColor = {Color.red, Color.black, Color.black, Color.blue, Color.green};
	Color myColor;

	// Use this for initialization
	void Start () {
		myColor = BalloonColor [RandomColorNumber ()];
		rend.material.color = myColor;
		myScript = TreeObject.GetComponent<firTreeScript>();
		healthScript = TankObject.GetComponent<TankHealth> ();

	}
		


	void OnTriggerEnter(Collider other) {
		//check the color of the balloon
		GameObject.Destroy (gameObject);
		if (other.CompareTag ("bullet")) {
			GameObject water = Instantiate (particleSystem, transform.position, particleSystem.transform.rotation) as GameObject;
			water.GetComponent<waterScript> ().balloonColor = myColor;
			int shootBy = other.GetComponent<ShellExplosion> ().owner;

			//condition to add the score
			//check which tank have shot the green balloon
			if (shootBy == 2) {
				if (myColor.Equals (Color.green))
					scoreScript.redScore += 2;
			} else {
				if (myColor.Equals (Color.green))
					scoreScript.blueScore += 2; 
			}

			//check which tank have shot the black balloon, the evil balloon
			if (shootBy == 2) {
				if (myColor.Equals (Color.black))
					scoreScript.redScore -= 3;
			} else {
				if (myColor.Equals (Color.black))
					scoreScript.blueScore -= 3; 
			}

			if (myColor.Equals (Color.blue)) {
				scoreScript.blueScore += 1; 

			} else if (myColor.Equals (Color.red)) {
				scoreScript.redScore += 1;

			}
		}

		if (other.CompareTag ("Tank")) {
			healthScript.OnDeath ();
		}


	}




	// Update is called once per frame
	void FixedUpdate () {
		GetComponent<Rigidbody> ().AddForce (Vector3.down * downSpeed);
	}

	public int RandomColorNumber(){
		RandomNumber = (int)(UnityEngine.Random.Range (0, BalloonColor.Length));
		return RandomNumber;
	}


}
