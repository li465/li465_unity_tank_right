﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class scoreScript : MonoBehaviour {


	static public int redScore;
	static public int blueScore;

	public Text redText;
	public Text blueText;


	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
		redText.text = "Red: " + redScore;

		blueText.text = "Blue: " + blueScore;
	}
}
