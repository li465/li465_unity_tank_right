﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollowRotate : MonoBehaviour {

	[SerializeField]
	private Transform target;

	[SerializeField]
	private Vector3 offsetposition;

	[SerializeField]
	private Space offsetPositionSpace = Space.Self;

	[SerializeField]
	private bool lookAt = true;

	
	// Update is called once per frame
	private void LateUpdate () {
		Refresh ();
	}

	public void Refresh (){
		if (target == null) {
			Debug.LogWarning ("Missing target ref !", this);

			return;
		}

		//compute position
		if(offsetPositionSpace == Space.Self){
			transform.position = target.TransformPoint(offsetposition);
		}else
			transform.position = target.position + offsetposition;


		//compute rotation
		if (lookAt)
			transform.LookAt (target);
		else
			transform.rotation = target.rotation;
	}
}
